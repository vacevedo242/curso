export const environment = {
    production: false,
    apiSivet: 'http://localhost:3001',
    urlServer : "http://192.168.254.4:4200",
    initialRoute: 'https://datastudio.google.com/embed/reporting/b474160b-67a5-417a-bcf4-a430e979e331/page/xyP0C',
    urlSocket : "http://192.168.254.2:9090"
};
