export const environment = {
  production: true,
  // RUTAS DE LA NUBE
  // apiSivet: 'https://dev-apis.inabve.sv/v1/api_sivetweb',
  // urlServer : "https://dev.inabve.sv",
  // initialRoute: 'https://datastudio.google.com/embed/reporting/b474160b-67a5-417a-bcf4-a430e979e331/page/xyP0C',
  // // initialRoute: 'https://dev.core.inabve.gob.sv/registro',
  // urlSocket : "https://dev-apis.inabve.sv/"

  // RUTAS INTERNAS
  apiSivet: 'https://dev.apis.inabve.gob.sv/v1/api_sivetweb',
  urlServer : "https://dev.core.inabve.gob.sv",
  initialRoute: 'https://datastudio.google.com/embed/reporting/b474160b-67a5-417a-bcf4-a430e979e331/page/xyP0C',
  //initialRoute: 'https://app.powerbi.com/view?r=eyJrIjoiZThlZjk0YjItMDI1OC00ZWJmLTg2OTgtODU5MGIxMTU4ODdlIiwidCI6ImE4NDBmNjVjLTE3NzItNDhmYy04MjQwLWFlYjIyZjE3NGNmMCJ9',
  // initialRoute: 'https://dev.core.inabve.gob.sv/registro',
  urlSocket : "https://dev.socket.inabve.gob.sv/"
};
