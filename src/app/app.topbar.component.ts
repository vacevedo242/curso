import { style } from '@angular/animations';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppMainComponent } from './app.main.component';
import { NotificacionesComponent } from './components/notificaciones/notificaciones.component';
import { NotificacionesService } from './services/notificaciones.service';
import { EstadoNotificacionLeida, NotSubtractHoursDate, socket, _Crud } from './misc/constants';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import Swal from 'sweetalert2';
import { filter } from 'rxjs/operators';
import { MessageService } from 'primeng/api';
import { UserService } from './services/user.service';
import { SegUsuariosModel } from './models/seg_user_model';
import { NgxSpinnerService } from 'ngx-spinner';  

@Component({
    selector: 'app-topbar',
    templateUrl: './app.topbar.component.html',
    styleUrls: ['./app.topbar.component.scss'],
    providers: [DialogService, MessageService]
})
export class AppTopBarComponent implements OnInit, AfterViewInit {
    visibleNotifications : boolean = false
    visibleDialogo : boolean = false
    lsNotificacionesAux : any [] = []
    lsNotificaciones : any [] = []
    countNotifications : string = '0'
    stageCount : number
    btnAcciones : any [] = []
    ref: DynamicDialogRef
    param : any
    @ViewChild(NotificacionesComponent) notificaciones : NotificacionesComponent;
    @ViewChild('btn_notificacion') btn_notificacion : ElementRef<HTMLElement>;

    conSonido : boolean = false
    conMensaje : boolean = false
    user:String=localStorage.getItem("user");
    usuarioModel:SegUsuariosModel
    session:boolean=false;

    constructor(
        private router: Router,
        public app: AppMainComponent,
        public dialogService: DialogService,
        private _service : NotificacionesService,
        private messageService: MessageService,
        public _user: UserService,
        public spinner: NgxSpinnerService
    ) {}

    ngOnInit(): void {
        // socket.connect()
        // this.detectarConfiNotificacion()
        // this.showNotificationsList()
        // this.verificarSesion();
    }
    ngAfterViewInit(): void {
        this.detectarConfiNotificacion()
        this.showNotificationsList()
        this.verificarSesion();
    }

    detectarConfiNotificacion() {
        this.conSonido = localStorage.getItem('conSonido') == null ? false : localStorage.getItem('conSonido') == 'true' ? true : localStorage.getItem('conSonido') == 'false' ? false : false
        this.conMensaje = localStorage.getItem('conMensaje') == null ? false : localStorage.getItem('conMensaje') == 'true' ? true : localStorage.getItem('conMensaje') == 'false' ? false : false
    }

    verificarConexionSocket(){
        // socket.on("conexionEstablecida",(status)=>{
        // //    console.log('Conectado : ' + status);
        // })
    }

    mostrarNotificaciones(){
        this.visibleNotifications = !this.visibleNotifications
        // this.desconectar()
        // this.showNotificationsList()
        // this.lsNotificaciones = []
        // this.lsNotificaciones = [...this.lsNotificacionesAux]
    }

    ngOnDestroy(): void {
       this.desconectar()
    }

    desconectar() {
        socket.disconnect()
        socket.close()
    }

    showNotificationsList() {
        socket.connect();
        const idUsuario = localStorage.getItem('idUser')
        // console.log(idUsuario);
        socket.emit('traerNotificaciones',idUsuario)
        socket.on(`notificacionesData${idUsuario}`, (notificaciones: any) => {
            this.stageCount = parseInt(notificaciones.data.length.toString())
            if(notificaciones.data.length > 0){
                notificaciones.data.forEach(noti => {
                    noti.creadoEn = NotSubtractHoursDate(noti.creadoEn)
                });
                if (Number(this.countNotifications) != this.lsNotificaciones.length) {
                    this.lsNotificaciones = [...notificaciones.data]
                }
                if(this.stageCount > Number(this.countNotifications)){
                    if (this.conSonido) {
                        this.triggerFalseClick()
                        let el: HTMLElement = this.btn_notificacion.nativeElement;
                        el.focus()
                        el.click()
                        this.playAudio()
                    }
                    if (this.conMensaje) {
                        this.triggerFalseClick()
                        let el: HTMLElement = this.btn_notificacion.nativeElement;
                        el.focus()
                        el.click()
                        this.mostrarToast()
                    }
                }
                // seteamos el contador de notificaciones para que la siguiente llamada se cumpla la condicion
                this.countNotifications = notificaciones.data.length.toString()
                // console.log(this.lsNotificaciones);
            } else if (notificaciones.data.length == 0) {
                this.countNotifications = '0'
                this.lsNotificaciones = []
            }
        })
    }

    triggerFalseClick() {
        let el: HTMLElement = this.btn_notificacion.nativeElement;
        el.focus()
        el.click()
    }

    mostrarDialogNotificacion(notificacion?: any) {

        this._service.obtenerNotification( notificacion.id ).subscribe(notificacion => {
            if (notificacion.message == _Crud.CRUD200 ) {
                // this.btnAcciones = []
                this.obtenerConfiguracion(notificacion.data[0])
                //console.log(lsAcciones);
            } else{
                Swal.fire("Información", "No se pudo obtener la notificación", "info")
            }
        },
        error => {
            console.log('Error: ' + error);
        })


    }

    obtenerConfiguracion(notificacion : any) {
        this.ref = this.dialogService.open(NotificacionesComponent, {
            data: {
                notificacion: notificacion,
            },
            header: "Notificación nueva",
            width: "40%",
            contentStyle: { "max-height": "60vh", overflow: "auto" },
            closeOnEscape: false,
        });

        this.ref.onClose.subscribe((bandera: any) => {
            if (bandera != null && bandera.proceso) {
                this.triggerFalseClick()
                //Swal.fire("Información", "Se han guardado los cambios!", "success");
            }
            if (bandera != null && bandera.leido) {
                if (this.conSonido) {
                    this.triggerFalseClick()
                    this.playAudioErase()
                }
            }
        });
    }

    playAudioErase() {
        let audio = new Audio();
        audio.src = "https://core.inabve.gob.sv/veteran_photos/api/public/dl/seBo8shb?inline=true";
        audio.load();
        audio.play();
    }

    playAudio() {
        let audio = new Audio();
        audio.src = "https://core.inabve.gob.sv/veteran_photos/api/public/dl/6PaMkMrY?inline=true";
        // audio.src = "../assets/img/Bell.wav";
        audio.load();
        audio.play();
    }

    mostrarToast() {
        this.messageService.add({severity:'custom', summary: 'Notificación', detail: 'Nueva notificación', icon: 'pi-send'});
    }

    guardarSonido() {
        localStorage.setItem('conSonido', this.conSonido.toString())
    }

    guardarMensaje() {
        localStorage.setItem('conMensaje', this.conMensaje.toString())
    }

    verTodasNotificaciones() {

    }

    logout(){
        this.spinner.show();
        let usr=localStorage.getItem("user")
        let socketListener = socket.hasListeners(`estadoSesion${usr}`);
        if(socketListener){
            socket.removeAllListeners();
            socket.disconnect()
        }
        this._user.logout(localStorage.getItem("user"))
        .subscribe((data:any)=>{
          
            socket.removeAllListeners();
            socket.disconnect();
        
        //   this.router.navigate(["/login"]);
          this.spinner.hide();
          location.reload();
          //this._menu.menuObj = [];
        })

      }

      verificarSesion() {
        let usr=localStorage.getItem("user")
        this.session=false;
        socket.emit('sesionActiva',usr);
        socket.on(`estadoSesion${usr}`,(estadoSesion:any)=>{
            this.usuarioModel=estadoSesion.data[0];
            // console.log(this.usuarioModel);
            if(this.usuarioModel.estadoSesion == false){
                if(this.session){
                    return;
                }else{
                    this.session=true;
                }
            }
            if(this.session){
                let _this=this;
                localStorage.clear();
                sessionStorage.clear();
                Swal.fire({
                    allowOutsideClick:false,
                    title:"Administrador de sesión",
                    text:"Estimado "+usr+" se ha desactivo la sesión al sistema.",
                    icon:'info',
                    confirmButtonText:"Aceptar",
                    showConfirmButton:false,
                    timer:4000,
                    timerProgressBar:true
                })
                .then((r)=>{
                    if(r.dismiss==Swal.DismissReason.timer){
                        location.reload();
                    }
                })
            }
        })
      //  Swal.fire('Prueba','Conexion perdida','warning')
    }
    reloadComponent() {
        let currentUrl = this.router.url;
            this.router.routeReuseStrategy.shouldReuseRoute = () => false;
            this.router.onSameUrlNavigation = 'reload';
            this.router.navigate(['/']);
        }


    buscarNotificaciones() {

    }

    marcarComoLeidoTodo() {
        Swal.fire({
            title: "¿Esta seguro de continuar?",
            text:`Una vez hecho no podrá realizar acciones a las notificaciones`,
            icon:'info',
            confirmButtonText:'Continuar',
            confirmButtonColor: 'green',
            cancelButtonText: 'Cancelar',
            showCancelButton: true
        }).then(resp => {
            if (resp.value) {
                this.lsNotificaciones.forEach(notificacion => {
                    notificacion.idEstado = EstadoNotificacionLeida.id
                })
                this.spinner.show()
                this._service.marcarComoLeida(this.lsNotificaciones).subscribe({
                    next: (resp) => {
                        this.spinner.hide()
                        if (resp.message == _Crud.CRUD202 && resp.data.length > 0) {
                            Swal.fire("Información", "Marcados como leído", "success")
                            this.ref.close({proceso: true, leido: true})
                        } else {
                            Swal.fire("Información", "No se marcó como leído todos los registros", "error")
                        }
                    }, error: (err) => {
                        this.spinner.hide()
                        console.log("Error : " + err);
                    }
                })
            }
        })
    }
}
