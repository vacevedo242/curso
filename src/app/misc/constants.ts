import { HttpHeaders } from "@angular/common/http";
import { io } from "socket.io-client";
import { environment } from "src/environments/environment";
import Swal from "sweetalert2";

export const header = new HttpHeaders({
    'content-type'                : 'application/json',
    'Authorization'               : "Bearer "+localStorage.getItem('token')
  })

  export const ENTORNO = "SIVET-WEB"
  export const MenuPermisosMain = "Principal"

export const colorAccNotificaciones = [
  { classColor : 'p-button-primary', label : 'APROBAR', value : '14136'},
  { classColor : 'p-button-danger', label : 'RECHAZAR', value : '14139'},
  { classColor : 'p-button-help', label : 'LEIDO', value : '14137'},
  { classColor : 'p-button-success', label : 'MARCAR LEIDO', value : ''},
  { classColor : 'p-button-secondary', label : 'VER ORIGEN', value : '14138' }
]

export const EstadoNotificacionAprobada = {id : 14136, descripcion: 'APROBADO'}
export const EstadoNotificacionReviada = {id : 14137, descripcion: 'REVISADO'}
export const EstadoNotificacionLeida = {id : 14138, descripcion: 'VISTO'}
export const EstadoNotificacionNoLeida = {id : 14139, descripcion: 'NO LEIDO'}
export const EstadoNotificacionRechazada = {id : 14184, descripcion: 'RECHAZADO'}

export const socket = io( environment.urlSocket , { path: '/socket/socket.io',
    transports: ['websocket', 'polling'], autoConnect: false, upgrade : false,
  });



  export enum _Auth
{
	AUTH400	= "YouAreLost",
	AUTH401	= 'EmailPassRequired',
	AUTH402	= 'EmailPassWrong',
	AUTH403	= 'OldNewPassRequired',
	AUTH404	= 'SomethingIsWrong',
	AUTH405	= 'OldPwdDoesntMatch',
	AUTH406	= 'PendingApproval',
	AUTH407	= 'MissingDocumentation',
	AUTH408	= 'TemporarilySuspended',
	AUTH409	= 'BlockedOut',
	AUTH410 = 'EmailNotRegistered',
	AUTH411 = 'ConfirmPwdDoesntMarch',
	AUTH412 = 'EmailRegistered',
	AUTH201	= "SuccessLogin",
	AUTH202	= "SuccessLogout",
	AUTH203	= 'PassChanged',
	AUTH413 = 'ActiveLogin'
}

export enum _Jwt
{
	JWT400	= 'NotAuthorized'
}

export enum _Crud
{
	CRUD400	= 'NoResult',
	CRUD401	= 'AlreadyExist',
	CRUD402	= 'NotFound',
	CRUD403	= 'NoParams',
	CRUD200	= 'ResultData',
	CRUD201	= 'RecordCreated',
	CRUD202	= 'RecordUpdated',
	CRUD203	= 'RecordDeleted',
	CRUD204	= 'PasswordChanged'
}

export function NotSubtractHoursDate(s: string) {
    let fecha = new Date(new Date(s).getTime() + (new Date(s).getTimezoneOffset() * 60 * 1000));
    return fecha
    // let b: any = s.split(/\D+/);
    // console.log(b);
    // return new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5], b[6]));
}

export function SendDateHours(fecha: Date) {
	let fechaAux = new Date(fecha)
    let dia, mes, anio, hora, minuto, segundo
    dia = fechaAux.getDate()
    mes = fechaAux.getMonth()
    anio = fechaAux.getFullYear()
    hora = fechaAux.getHours()
    minuto = fechaAux.getMinutes()
    segundo = fechaAux.getSeconds()
    let fechaEnviar = new Date(Date.UTC(anio, mes, dia, hora, minuto, segundo))
    return fechaEnviar
}

export const TIEMPO_MENSAJE = 5000
export function TOKEN_EXPIRO() {
    Swal.fire({
        title: '¡Alerta!',
        text: "Su Sesión ha expirado, por favor inicie sesión",
        icon: "warning",
        timer: TIEMPO_MENSAJE,
        allowOutsideClick: false,
        timerProgressBar: true,
        showConfirmButton: false
        }).then((result) => {
    })

}



