import { Injectable } from '@angular/core'
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { JwtHelperService } from '@auth0/angular-jwt'
import { NgxSpinnerService } from 'ngx-spinner'
import { Router } from '@angular/router'
import { TIEMPO_MENSAJE, TOKEN_EXPIRO } from './constants'

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(
        public _spinner : NgxSpinnerService,
        private router: Router,
    ) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
    {
        if (localStorage.getItem('token') || localStorage.getItem('token') !== null) {
            // validamos si el token es valido y no ha vencido
            if (this.tokenExpirado()) {
                return null
            }
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
        }
        return next.handle(request).pipe(map( (val: HttpEvent<any>) => {
            if (val instanceof HttpResponse)
            {
                // TODO: You can do anything with body response
                const body = val.body
            }
            return val
        }))
    }

    tokenExpirado() {
        // debugger
        let expirado: boolean = true
        const helper = new JwtHelperService()
        if (localStorage.getItem('token') != null) {
          try {
            const decodedToken = helper.decodeToken(localStorage.getItem('token'))
            if (decodedToken! && decodedToken != null) {
              expirado = helper.isTokenExpired(localStorage.getItem('token'))
            }

          } catch (error) {
            expirado = true
          }
        }

        if (expirado) {
          this._spinner.hide()
          TOKEN_EXPIRO()
          setTimeout(() => {
            localStorage.clear()
            this.router.navigate(['/login'])
          }, TIEMPO_MENSAJE);
        }

        return expirado
    }
}
