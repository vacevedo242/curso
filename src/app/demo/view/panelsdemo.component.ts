import {Component, OnInit} from '@angular/core';
import {MenuItem} from 'primeng/api';
import {BreadcrumbService} from '../../app.breadcrumb.service';
import { Customer } from '../domain/customer';
import { CustomerService } from '../service/customerservice';

@Component({
    templateUrl: './panelsdemo.component.html',
    styles: [`
		:host ::ng-deep button {
			margin-right: .25em;
			margin-left: .25em;
		}

		:host ::ng-deep .p-splitbutton button {
			margin-right: 0;
			margin-left: 0;
		}

        :host ::ng-deep .p-splitter-panel-nested {
            overflow: auto;
        }

        @media screen and (max-width: 960px) {
            .card.toolbar-demo {
                overflow: auto;
            }
        }
    `]
})
export class PanelsDemoComponent implements OnInit {

    items: MenuItem[];

    cardMenu: MenuItem[];

    selectedState: any = null;

    customers1: Customer[];
    selectedCustomers1: Customer[];

    states: any[] = [
        {name: 'Arizona', code: 'Arizona'},
        {name: 'California', value: 'California'},
        {name: 'Florida', code: 'Florida'},
        {name: 'Ohio', code: 'Ohio'},
        {name: 'Washington', code: 'Washington'}
    ];

    constructor(private customerService: CustomerService, private breadcrumbService: BreadcrumbService) {
        this.breadcrumbService.setItems([
            {label: 'UI Kit'},
            {label: 'Panel'}
        ]);
    }

    ngOnInit() {
        this.items = [
            {label: 'Angular.io', icon: 'pi pi-external-link', url: 'http://angular.io'},
            {label: 'Theming', icon: 'pi pi-bookmark', routerLink: ['/theming']}
        ];

        this.cardMenu = [
            {
                label: 'Save', icon: 'pi pi-fw pi-check'
            },
            {
                label: 'Update', icon: 'pi pi-fw pi-refresh'
            },
            {
                label: 'Delete', icon: 'pi pi-fw pi-trash'
            },
        ];

        this.customerService.getCustomersLarge().then(customers => {
            this.customers1 = customers;
            // @ts-ignore
            this.customers1.forEach(customer => customer.date = new Date(customer.date));
        });
    }
}
