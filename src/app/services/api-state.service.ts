import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { header } from '../misc/constants';
import { ObjState } from '../models/objStatus';

@Injectable({
  providedIn: 'root'
})
export class ApiStateService {
  idUser : number = -1

  constructor(
    private _http   :    HttpClient,
  ) { }

  public guardarState(obj: any): Observable<any> {
    return this._http.post<any>(environment.apiSivet+'/states/create', obj, {
        'headers':header}).pipe(
            map((resp: any) => { 
                return resp;
            }),
            catchError(err => {
                console.log(err)
                return err; 
            })
        )
    }

    public obtenerState(idUser: number): Observable<any> {
      return this._http.post<any>(environment.apiSivet+'/states/getByUser', { idUser: idUser }, {
          'headers':header}).pipe(
              map((resp: any) => { 
                  return resp;
              }),
              catchError(err => {
                  console.log(err)
                  return err; 
              })
          )
      }

  obtenerYGuardarState(entorno? : string) {
    this.idUser = Number(localStorage.getItem("idUser"))
    // obtenemos la APIState para no perder los datos que ya se encuentran almacenados
    this.obtenerState(this.idUser).subscribe(resp => {
      
      if (resp.code == 1 && resp! && resp.states!) {
        let obStateNuevo : ObjState = resp.states
        // seteo el entorno y la otra informacion que obtenemos la dejamos tal cual
        obStateNuevo.entorno = entorno
        obStateNuevo.idUser = localStorage.getItem("idUser");

        this.guardarState(obStateNuevo).subscribe(result => {
          if (result!) {
          }
        }, err => {
    
        })

      }
      else if(resp.code == 1)
      {
        let objeto:any = {}
        objeto.entorno = entorno
        objeto.idUser = localStorage.getItem("idUser");
        this.guardarState(objeto).subscribe(resp => {})
      }
    }, err => {

    })
    
  }
}
