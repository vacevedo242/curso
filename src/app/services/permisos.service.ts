import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { catchError, map } from 'rxjs/operators';
import { header } from "../misc/constants";
import { responseModel } from "src/app/models/responseModel";

@Injectable({
  providedIn: 'root'
})
export class PermisosService {

  listaPermisos: any[] = []
  permisos: any = null 
  errorPermisos: boolean = false

  constructor(
    private _http   :    HttpClient,
  ) { }


  public obtenerPermisos(): Observable<responseModel> {
    return this._http.post<responseModel>(environment.apiSivet + '/seguridad/permisos/obtener', { "modulo" : "Principal" }, {'headers' : header})
    .pipe(
          map((resp: responseModel) => {
            //console.log(resp);
              if (resp != null && resp.data.length > 0) {
                this.permisos = {}
                
                resp.data.forEach(permiso => {
                  this.permisos[permiso.opcionTransaccion] = true
                });
                this.errorPermisos = false
              } else {
                this.permisos = null
                this.errorPermisos = true
              }
              return this.permisos;
          }),
          catchError(err => {
              console.log(err)
              this.permisos = null
              this.errorPermisos = true
              return err;
          })
        )
  }

}
