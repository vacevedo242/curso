import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'
import { environment } from "src/environments/environment";
import { header } from "../misc/constants";
import { catchError, map } from 'rxjs/operators';
import { regBitacoraModel } from '../models/reg_bitacora_model';

@Injectable({
  providedIn: 'root'
})
export class BitacoraService {

  constructor(private _http:HttpClient) { }
	public obtenerState(idUser: number): Observable<any> {
		return this._http.post<any>(environment.apiSivet + '/states/getByUser', { idUser: idUser }, {
			'headers': header
		}).pipe(
			map((resp: any) => {
				return resp;
			}),
			catchError(err => {
				console.log(err)
				return err;
			})
		)
	}

	public obtenerCatalogo(_tipo:string, _relacion:number): Observable<any> {
		const body:any = {
			tipo: _tipo,
			relacion: _relacion
		}
		return this._http.post<any>(environment.apiSivet + '/administracion/catalogos/obtener', body, {
			'headers': header
		}).pipe(
			map((resp: any) => {
				return resp;
			}),
			catchError(err => {
				console.log(err)
				return err;
			})
		)
	}

	public obtenerEstados(_relacion:number): Observable<any> {
		const body:any = {
			relacion: _relacion
		}
		return this._http.post<any>(environment.apiSivet + '/administracion/estados/obtener', body, {
			'headers': header
		}).pipe(
			map((resp: any) => {
				return resp;
			}),
			catchError(err => {
				console.log(err)
				return err;
			})
		)
	}

	public guardarBitacora(bitacoraObj:regBitacoraModel): Observable<any> {
		return this._http.post<any>(environment.apiSivet + '/registros/bitacora/guardar', bitacoraObj, {
			'headers': header
		}).pipe(
			map((resp: any) => {
				return resp;
			}),
			catchError(err => {
				console.log(err)
				return err;
			})
		)
	}

	public obtenerBitacora(_bitacora:any): Observable<any> {
		return this._http.post<any>(environment.apiSivet + '/registros/bitacora/obtener', _bitacora, {
			'headers': header
		}).pipe(
			map((resp: any) => {
				return resp;
			}),
			catchError(err => {
				console.log(err)
				return err;
			})
		)
	}

	public eliminarBitacora(id_bitacora:number): Observable<any> {
		const body:any = {
			id_bitacora: id_bitacora
		}
		return this._http.post<any>(environment.apiSivet + '/registros/bitacora/eliminar', body, {
			'headers': header
		}).pipe(
			map((resp: any) => {
				return resp;
			}),
			catchError(err => {
				console.log(err)
				return err;
			})
		)
	}

	public obtenerPersonaPorDUI(_dui:string): Observable<any> {
		const _particular = {
			dui: _dui
		}
		return this._http.post<any>(environment.apiSivet + '/registros/personas/obtener', _particular, {
			'headers': header
		}).pipe(
			map((resp: any) => {
				return resp;
			}),
			catchError(err => {
				console.log(err)
				return err;
			})
		)
	}
}
