import { ElementRef, Injectable, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { catchError, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { header } from "./../misc/constants";
import { MenuService } from '../app.menu.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  @ViewChild('mainIframe') iframe: ElementRef;

  constructor(
    private http: HttpClient,
    private _user: MenuService
  ) {

  }

  setearDatos(resp) {
    localStorage.setItem('token', resp.token)
    localStorage.setItem('user', resp.data.usuario)
    localStorage.setItem('idUser', resp.data.id)
    localStorage.setItem('idSucursal', resp.data.idSucursal)
    localStorage.setItem('idDepartment',resp.data.idDepartamento)
    localStorage.setItem('idArea', resp.data.idArea)
    localStorage.setItem('area', resp.data.area)
  }

  authentication(_user, _password,passTheTraffic?) {
    return this.http.post(environment.apiSivet + "/seguridad/login", {"usuario": _user, "clave": _password,"passTheTraffic":passTheTraffic,},{'headers': header})
      .pipe(
        map((resp: any) => {
          if (resp.token ) {
            this.setearDatos(resp)
            return resp;
          } else {
            return resp
          }
        }),
        catchError((err) => {
          // swal('Error.', err.error.exception.Message);
          //console.log(err);

          return err // Observable.throw(err);
        })
      );
  }

  reestablecerClave(usuario) {
    return this.http.post(environment.apiSivet + "/seguridad/reestablecer-clave", {usuario}, {'headers': header})
      .pipe(
        map((resp: any) => {
          if (resp.token ) {
            this.setearDatos(resp)
            return resp;
          } else {
            return resp;
          }
        }),
        catchError((err) => {
          return err
        })
      );
  }

  codigoSeguridad(usuario) {
    return this.http.post(environment.apiSivet + "/seguridad/codigo-seguridad", {usuario}, {'headers': header})
      .pipe(
        map((resp: any) => {
          if (resp.token ) {
            //this.setearDatos(resp)
            return resp;
          } else {
            return resp;
          }
        }),
        catchError((err) => {
          return err
        })
      );
  }

  verificarCodigoSeguridad(usuario) {
    return this.http.post(environment.apiSivet + "/seguridad/verificar-codigo-seguridad", usuario, {'headers': header})
      .pipe(
        map((resp: any) => {
          if (resp.token ) {
            //this.setearDatos(resp)
            return resp;
          } else {
            return resp;
          }
        }),
        catchError((err) => {
          return err
        })
      );
  }

  logout(_user) {
    return this.http.post(environment.apiSivet + "/seguridad/logout", {"usuario": _user, },{'headers': header})
      .pipe(
        map((resp: any) => {
          if (resp!=null ) {
            localStorage.clear();
            sessionStorage.clear();
            return resp;
          } else {
            return resp
          }
        }),
        catchError((err) => {
          // swal('Error.', err.error.exception.Message);
          //console.log(err);

          return err // Observable.throw(err);
        })
      );
  }

  obtenerAreasAtencion(){
    return this.http.post(environment.apiSivet + "/administracion/catalogos/obtener", {"tipo":"areasAtencion"} ,{'headers': header})
    .pipe(
        map((resp: any) => {
        if (resp.data ) {
          return resp.data;
        } else {
          return null
        }
    }),
    catchError((err) => {
        return err ;
    })
  );
}

}
