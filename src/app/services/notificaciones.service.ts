import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { header } from "./../misc/constants";

@Injectable({
  providedIn: 'root'
})
export class NotificacionesService {

  constructor(
    private http: HttpClient,
  ) { }



  public getNotifications( idUsuario ): Observable<any[]> {
    return this.http.post<any>(environment.apiSivet + "/notificaciones/notificaciones/obtener", { id: null }, { 'headers': header })
      .pipe(
        map((lsNotificaciones: any) => {
          // console.log(lsNotificaciones.data);
          if (lsNotificaciones.data.length > 0) {
            let ids = []
            let lista : any = []
            lsNotificaciones.data.forEach( e => {
              ids = e.destinos.split(',')
              ids.forEach( v => {
                if(v == idUsuario){
                  lista.push(e)
                }
              });
            });
            return lista;
          } else {
            return null
          }
        }),
        catchError((err) => {
          return err // Observable.throw(err);
        })
      );
  }


  obtenerNotification(idNotificacion: number) {
    return this.http.post<any>(environment.apiSivet + "/notificaciones/notificaciones/obtener", {id : idNotificacion},{ 'headers': header })
      .pipe(
        map(resp => {
          return resp
        }),
        catchError((err) => {
          return err // Observable.throw(err);
        })
      );
  }

  notificationResponse( notificacion : any, accion : any, comentario : string = ""){
    return this.http.post<any>(environment.apiSivet + "/notificaciones/accionesNotificaciones/guardar", { notificacion : notificacion, accion : accion, comentario : comentario }, { 'headers': header })
      .pipe(
        map((resp: any) => {
          // console.log(resp);
          if (resp) {
            return resp;
          } else {
            return null
          }
        }),
        catchError((err) => {
          return err // Observable.throw(err);
        })
      );
  }

  getNotificationAccion( param : object){
    return this.http.post<any>(environment.apiSivet + "/notificaciones/acciones/obtener", param, { 'headers': header })
      .pipe(
        map((lsAcciones: any) => {
          // console.log(lsAcciones);
          if (lsAcciones) {
            return lsAcciones.data;
          } else {
            return null
          }
        }),
        catchError((err) => {
          return err // Observable.throw(err);
        })
      );
  }

  marcarComoLeida(notificacion : any){
    return this.http.post<any>(environment.apiSivet + "/notificaciones/acciones/marcar-como-leido", notificacion, { 'headers': header })
      .pipe(
        map((resp: any) => {
          // console.log(resp);
          if (resp) {
            return resp;
          } else {
            return null
          }
        }),
        catchError((err) => {
          return err // Observable.throw(err);
        })
      );
  }

}
