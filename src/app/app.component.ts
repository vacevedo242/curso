import { ApiStateService } from './services/api-state.service';
import { MenuService } from './app.menu.service';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { PrimeNGConfig } from 'primeng/api';
import { environment } from 'src/environments/environment';
import { DomSanitizer } from '@angular/platform-browser';
import { SegUsuariosModel } from './models/seg_user_model';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { NgxSpinnerService } from 'ngx-spinner';
import { ENTORNO } from './misc/constants';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {


    topbarColor = 'layout-topbar-inabve';

    menuColor = 'layout-menu-light';

    themeColor = 'blue';

    layoutColor = 'blue';

    topbarSize = 'large';

    horizontal = true;

    inputStyle = 'outlined';

    ripple = true;

    compactMode = false;

    urlState: string = ''

    constructor(
        private primengConfig: PrimeNGConfig,
        private _menu: MenuService,
        private sanitizer: DomSanitizer,
        private router: Router,
        public _spinner: NgxSpinnerService,
        private _apiState : ApiStateService
    ) { }

    ngOnInit() {
        this.primengConfig.ripple = true;
        // localStorage.setItem('entorno', ENTORNO);
        // if (localStorage.getItem("token")) {
        //     // this.obtenerUrlStatus()
        //     let url = localStorage.getItem('route') || environment.initialRoute
        //     this.obtenerIframe(url)
        //     // this._apiState.obtenerYGuardarState(ENTORNO)
        // }
    }

    obtenerUrlStatus() {
        // let user = parseInt(localStorage.getItem('idUser'))
        // this._menu.obtenerState(user).subscribe(
        //     (obj: any) => {
        //         if (obj.states != null) {
        //             if(obj.states.infoVivienda != null){
        //                 this.urlState = obj.states.infoVivienda.ruta
        //             }
        //         }
        //         this.obtenerIframe(this.urlState)
        //     }, (error) => {

        //     }, () => {

        //     })
    }

    obtenerIframe(urlStr: any) {
        // let url = urlStr != '' ? urlStr : localStorage.getItem('route') || environment.initialRoute
        // this._menu.currentRoute = this.sanitizer.bypassSecurityTrustResourceUrl(url)
        // setTimeout(() => {
        //     this._menu.refreshIframe(url)
        // }, 500);

        // this._menu.getMenu().subscribe((menu) => {
        //     this._menu.menuObj = []
        //     this._menu.menuObj.push(
        //         {
        //             label: 'Inicio',
        //             icon: 'pi pi-home',
        //             urlOpt: environment.initialRoute,
        //             descripcion: 'Inicio'
        //         }
        //     )
        //     menu.data.forEach(option => {
        //         this._menu.menuObj.push(
        //             {
        //                 label: option.nombre,
        //                 icon: option.icono,
        //                 urlOpt: option.url,
        //                 descripcion: option.descripcion
        //             }
        //         )
        //     });

        // }, error => { }
        // )
    }

}
