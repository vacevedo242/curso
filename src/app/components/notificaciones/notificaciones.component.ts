import { Component, EventEmitter, Input, OnInit, Output, ViewChild, AfterViewInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxSpinnerService } from 'ngx-spinner';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Fieldset } from 'primeng/fieldset';
import { MenuService } from 'src/app/app.menu.service';
import { colorAccNotificaciones, EstadoNotificacionLeida, _Crud } from 'src/app/misc/constants';
import { adminAccNotificacionesModel } from 'src/app/models/admin_acc_notificaciones';
import { adminNotificacionesModel } from 'src/app/models/admin_notificaciones';
import { NotificacionesService } from 'src/app/services/notificaciones.service';
import Swal from 'sweetalert2';

  Fieldset
  @Component({
      selector: 'app-notificaciones',
      templateUrl: './notificaciones.component.html',
      styleUrls: ['./notificaciones.component.scss']
  })
export class NotificacionesComponent implements OnInit {

    showNotification : boolean = false

    notificacion : any

    btnAcciones : any[] = []
    accion : adminAccNotificacionesModel = new adminAccNotificacionesModel()

    btnVisible : boolean = false

    modalDenegar : boolean = false
    comentario : string = ""
    btnAux : adminAccNotificacionesModel = new adminAccNotificacionesModel()
    // @ViewChild('body_notificacion') body_notificacion : HTMLElement

    idNotificacionLeida = EstadoNotificacionLeida.id

    constructor(
        private _service : NotificacionesService,
        public ref      : DynamicDialogRef, 
        public config   : DynamicDialogConfig,
        public dialogService: DialogService,
        public _menu: MenuService,
        private sanitizer: DomSanitizer,
        private _spinner : NgxSpinnerService
    ) { }
  
    ngOnInit(): void {
        if (this.config.data != null) {
            
            if (this.config.data.notificacion != null) {
                // buscamos la notificacion y sus acciones<
                this.notificacion = this.config.data.notificacion
                this.btnAux = this.notificacion.btnAcciones
                // this.obtenerNotificacion({'id_conf_notificacion' : this.config.data.notificacion.idConfNotificacion})
            }
        }else{
          console.log('vacio');
        }
    }

    cerrarDialog(){
        this.showNotification = false
        this.notificacion = []
        this.btnAcciones = []
    }

    // obtenerNotificacion(idNotificacion) {
    //     this._service.obtenerNotification( idNotificacion ).subscribe(notificacion => {
    //         if (notificacion.message == _Crud.CRUD200 ) {    
    //             // this.btnAcciones = [] 
    //             this.notificacion = notificacion.data[0]
    //             this.btnAux = this.notificacion.btnAcciones
    //             //console.log(lsAcciones);
    //         } else{
    //             Swal.fire("Información", "No se pudo obtener la notificación", "info")
    //         }
    //     },
    //     error => {
    //         console.log('Error: ' + error);
    //     })
    // }
    
    cambiarEstadoNotificacion(notificacion : adminNotificacionesModel, accion : adminAccNotificacionesModel ){
        // console.log(accion);
        this.accion = accion
        let notificacionAux = {...notificacion}
        notificacionAux.idEstado = accion.idEstadoAsociado
        if (accion.nombre == 'Denegar' || accion.nombre == 'Rechazar' || accion.nombre == 'Eliminar') {
            // Swal.fire({
            //     title: accion.descripcion,
            //     text:`¿Esta seguro de continuar?`,
            //     icon:'warning',
            //     confirmButtonText:'Continuar',
            //     confirmButtonColor: 'crimson',
            //     cancelButtonText: 'Cancelar',
            //     showCancelButton: true
            //   }).then(resp => {
            //     if (resp.value) {
                    
            //     }
            // })
            this.modalDenegar = true
            
        } else if (accion.nombre == 'Aprobar' || accion.nombre == 'Revisar' || accion.nombre == 'Autorizar') {
            
            Swal.fire({
                title: accion.descripcion,
                text:`¿Esta seguro de continuar?`,
                icon:'info',
                confirmButtonText:'Continuar',
                confirmButtonColor: 'green',
                cancelButtonText: 'Cancelar',
                showCancelButton: true
              }).then(resp => {
                if (resp.value) {
                    this.guardarNotificacion(notificacionAux, accion)
                }
              })
            
        } else {
            this.guardarNotificacion(notificacionAux, accion)

        }
       
    }

    guardarNotificacion(notificacion : adminNotificacionesModel, accion : any) {
        notificacion.idEstado   = notificacion.idEstado -1 ? notificacion.idEstado : -1
        this._service.notificationResponse(notificacion, accion, this.comentario).subscribe(
            resp =>{
                if (resp.message == _Crud.CRUD200){
                    if (this.modalDenegar) {
                        Swal.fire("Información", "Realizado correctamente", "success")

                    } else {
                        Swal.fire("Información", "Notificacion enviada correctamente", "success")
                    }
                    this.ref.close({proceso: true, leido: false})
                } else if (resp.message == _Crud.CRUD202){
                    Swal.fire("Información", "Marcado como leido", "success")
                    this.ref.close({proceso: true, leido: true})
                } else {
                    Swal.fire("Información", "Ocurrio un problema", "error")
                }
            }, 
            error => {
                console.log("Error : " + error);
            }
        )
    }

    VerOrigen(notificacion: adminNotificacionesModel) {
        //this._menu.currentRoute = notificacion.url
        this.ref.close(false)
        localStorage.setItem('route', '')
        this._menu.onMenuStateChange('')
        this._menu.onMenuStateChange(notificacion.url)
    }

    marcarComoLeido(notificacion : adminNotificacionesModel) {
        Swal.fire({
            title: "¿Esta seguro de continuar?",
            text:`Una vez hecho no podrá realizar acciones a la notificación`,
            icon:'info',
            confirmButtonText:'Continuar',
            confirmButtonColor: 'green',
            cancelButtonText: 'Cancelar',
            showCancelButton: true
        }).then(resp => {
            if (resp.value) {
                notificacion.idEstado = EstadoNotificacionLeida.id
                this._spinner.show()
                let listaNotificaciones = []
                listaNotificaciones.push(notificacion)
                this._service.marcarComoLeida(listaNotificaciones).subscribe({
                    next: (resp) => {
                        this._spinner.hide()
                        if (resp.message == _Crud.CRUD202 && resp.data.length > 0) {
                            Swal.fire("Información", "Marcado como leído", "success")
                            this.ref.close({proceso: true, leido: true})
                        } else {
                            Swal.fire("Información", "No se marcó como leído", "error")
                        }
                    }, error: (err) => {
                        this._spinner.hide()
                        console.log("Error : " + err);
                    }
                })
            }
        })
    
    }

}
