import { HttpClient } from '@angular/common/http';
import { ElementRef, Inject, Injectable, ViewChild } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { allowedNodeEnvironmentFlags, env } from 'process';
import { Observable, Subject } from 'rxjs';
import { environment } from "src/environments/environment";
import { responseModel } from "./../app/models/responseModel";
import { segMenusModel } from './models/seg_menus.Model';
import { header, MenuPermisosMain } from "./misc/constants";
import { catchError, map } from 'rxjs/operators';
import { DOCUMENT } from '@angular/common';


@Injectable()
export class MenuService {

    private menuSource = new Subject<string>();
    private resetSource = new Subject();

    menuObj: any[] = [];
    currentRoute: SafeResourceUrl = localStorage.getItem('route') || environment.initialRoute
    //currentRoute: SafeResourceUrl = this.sanitizer.bypassSecurityTrustResourceUrl('http://192.168.3.12:8090/#/veteranos/')

    menuSource$ = this.menuSource.asObservable();
    resetSource$ = this.resetSource.asObservable();

    constructor(
        private sanitizer: DomSanitizer,
        private _http: HttpClient,
        @Inject(DOCUMENT) private document: HTMLDocument
    ){
        

        let url = localStorage.getItem('route') || environment.initialRoute
        this.currentRoute = this.sanitizer.bypassSecurityTrustResourceUrl(url)
    }

    onMenuStateChange(newUrl: string) {
        //this.currentRoute = this.sanitizer.bypassSecurityTrustResourceUrl(newUrl)
        if (isNaN(Number(newUrl)))
        {
            if(newUrl !== localStorage.getItem('route'))
            {
                localStorage.setItem("route", newUrl);
                this.menuSource.next(newUrl)
                this.refreshIframe(newUrl)
            }
        }

    }

    public getMenu() {
        return this._http.post<any>(environment.apiSivet+'/seguridad/menu/obtener', { "modulo" : MenuPermisosMain }, {
            'headers':header}).pipe(
                map((resp: any) => {
                    //localStorage.setItem('token', resp.token)
                    return resp;
                }),
                catchError(err => {
                    console.log(err)
                    return err;
                })
            )
    }

    refreshIframe(url: string)
    {
        let me = this
        let iframe : any = this.document.getElementById('mainIframe')
        // let route = localStorage.getItem('route')
        if (isNaN(Number(url)))
        {
            iframe.src = url // this.iframe.nativeElement.src
            let ifr = iframe.contentWindow || iframe.contentDocument
            iframe.addEventListener('load', function(){
                setTimeout(() => {
                    ifr.postMessage(JSON.stringify({token: localStorage.getItem('token'), 
                    user: localStorage.getItem('user'), idUser: localStorage.getItem('idUser'),
                    idDepartment:localStorage.getItem("idDepartment")}), 
                    url);
                }, 1000);
                
            })
            /* if(url != route)
            { */
           /*  } */
        }
    }

    reset() {
        this.resetSource.next();
    }

    public guardarState(obj: any): Observable<any> {
		return this._http.post<any>(environment.apiSivet + '/states/create', obj, {
			'headers': header
		}).pipe(
			map((resp: any) => {
				return resp;
			}),
			catchError(err => {
				console.log(err)
				return err;
			})
		)
	}

	public obtenerState(idUser: number): Observable<any> {
		return this._http.post<any>(environment.apiSivet + '/states/getByUser', { idUser: idUser }, {
			'headers': header
		}).pipe(
			map((resp: any) => {
				return resp;
			}),
			catchError(err => {
				console.log(err)
				return err;
			})
		)
	}
}
