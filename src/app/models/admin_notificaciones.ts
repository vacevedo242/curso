export class adminNotificacionesModel
{
	id : number
	idConfNotificacion : number
	idUsuarioOrigen : number
	idUsuarioDestino : number
	idEstado : number | null
	idProceso : number | null
	comentarios : string | null
	mensajeNotif : string | null
	url : string | null
	creadoEn : Date
	creadoPor : number
	modificadoEn : Date
	modificadoPor : number
	activo : boolean

	constructor()
	{
		this.id = -1
		this.idConfNotificacion = -1
		this.idUsuarioOrigen = -1
		this.idUsuarioDestino = -1
		this.idEstado = -1
		this.idProceso = -1
		this.comentarios = ''
		this.mensajeNotif = ''
		this.url = ''
		this.creadoEn = new Date()
		this.creadoPor = -1
		this.modificadoEn = new Date()
		this.modificadoPor = -1
		this.activo = true
	}
}