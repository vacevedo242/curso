export class SegUsuariosModel
{
	id : number
	idDepartamento : number
	idSucursal : number
	usuario : string
	clave : string
	nombres : string
	apellidos : string
	ultimoLogin : Date
	bloqueado : number
	loginFallido : number
	estadoSesion : boolean
	creadoEn : Date
	creadoPor : number
	modificadoEn : Date
	modificadoPor : number
	activo : boolean

    claveRepetida? : string

	constructor()
	{
		this.id = -1
		this.idDepartamento = -1
		this.idSucursal = 24212 // INABVE CENTRAL
		this.usuario = ''
		this.clave = ''
		this.nombres = ''
		this.apellidos = ''
		this.ultimoLogin = new Date()
		this.bloqueado = 0
		this.loginFallido = 0
		this.estadoSesion = false
		this.creadoEn = new Date()
		this.creadoPor = -1
		this.modificadoEn = new Date()
		this.modificadoPor = -1
		this.activo = true

		this.claveRepetida = ''
	}
}
