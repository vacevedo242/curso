export class adminAccNotificacionesModel
{
	id : number
	idConfNotificacionHija : string | null
	idConfNotificacion : number
	idEstadoAsociado : number | null
	nombre : string
	descripcion : string | null
	icono : string | null
	colorBoton : string | null
	soloIcono : boolean
	creadoEn : Date
	creadoPor : number
	modificadoEn : Date
	modificadoPor : number
	activo : boolean

	constructor()
	{
		this.id = -1
		this.idConfNotificacionHija = ''
		this.idConfNotificacion = -1
		this.idEstadoAsociado = -1
		this.nombre = ''
		this.descripcion = ''
		this.icono = ''
		this.colorBoton = ''
		this.soloIcono = false
		this.creadoEn = new Date()
		this.creadoPor = -1
		this.modificadoEn = new Date()
		this.modificadoPor = -1
		this.activo = true
	}
}