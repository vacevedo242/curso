export class regBitacoraParticularesModel
{
	id : number
	dui : string
	nombres : string
	apellidos : string
	telefono : string
	comentarios : string
	creadoEn : Date
	creadoPor : number
	modificadoEn : Date
	modificadoPor : number
	activo : boolean

	constructor()
	{
		this.id = -1
		this.dui = ''
		this.nombres = ''
		this.apellidos = ''
		this.telefono = ''
		this.comentarios = ''
		this.creadoEn = new Date()
		this.creadoPor = -1
		this.modificadoEn = new Date()
		this.modificadoPor = -1
		this.activo = false
	}
}
