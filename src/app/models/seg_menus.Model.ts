export class segMenusModel
{
	id : number
	id_modulo : number
	id_rol : number
	id_padre : number
	nombre : string
	descripcion : string
	icono : string
	url : string
	activo : number
	creado_por : number
	creado_en : Date
	modificado_por : number
	modificado_en : Date

	constructor()
	{
		this.id = -1
		this.id_modulo = -1
		this.id_rol = -1
		this.id_padre = -1
		this.nombre = ''
		this.descripcion = ''
		this.icono = ''
		this.url = ''
		this.activo = 0
		this.creado_por = -1
		this.creado_en = new Date()
		this.modificado_por = -1
		this.modificado_en = new Date()
	}
}
