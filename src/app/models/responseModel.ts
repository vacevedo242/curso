export class responseModel
{
    message : string
    data : any
    token? : string

	constructor()
	{
		this.message = ''
		this.data = []
		this.token = ''
	}
}
