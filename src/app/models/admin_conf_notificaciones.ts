export class adminConfNotificacionesModel
{
	id : number
	idTipoNotificacion : string | null
	idTipoProceso : number | null
	idEstado : number | null
	nombre : string
	descripcion : string
	origen : number
	destinos : string | null
	mensaje : string
	url : string | null
	creadoEn : Date
	creadoPor : number
	modificadoEn : Date
	modificadoPor : number
	activo : boolean

	constructor()
	{
		this.id = -1
		this.idTipoNotificacion = ''
		this.idTipoProceso = -1
		this.idEstado = -1
		this.nombre = ''
		this.descripcion = ''
		this.origen = -1
		this.destinos = ''
		this.mensaje = ''
		this.url = ''
		this.creadoEn = new Date()
		this.creadoPor = -1
		this.modificadoEn = new Date()
		this.modificadoPor = -1
		this.activo = true
	}
}