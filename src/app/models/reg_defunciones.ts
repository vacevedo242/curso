export class RegDefuncionesModel
{
	id : number
	idVeterano : number | null
	idBeneficiario : number | null
	idDepartamento : number
	idMunicipio : number
	fecha : Date
	libro : string
	folio : string
	numPartida : string
	lugar : string
	causa : string
	creadoEn : Date
	creadoPor : number
	modificadoEn : Date
	modificadoPor : number

	constructor()
	{
		this.id = -1
		this.idVeterano = -1
		this.idBeneficiario = -1
		this.idDepartamento = -1
		this.idMunicipio = -1
		this.fecha = new Date()
		this.libro = ''
		this.folio = ''
		this.numPartida = ''
		this.lugar = ''
		this.causa = ''
		this.creadoEn = new Date()
		this.creadoPor = -1
		this.modificadoEn = new Date()
		this.modificadoPor = -1
	}
}