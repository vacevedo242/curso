import { regBitacoraParticularesModel } from "./reg_bitacora_particulares_model"

export class regBitacoraModel
{
	id : number
	idVeterano : number
	idBeneficiario : number
	idParticular: number
	idUnidad : number
	idUnidadUsuario: number | null
	idActividad : number
	idCanalAtencion: number
	idEstatus : number
	tipoLlamada : boolean
	origenLlamada : boolean | null
	fecha : Date
	descripcion : string | null
	horaInicio : Date
	horaFin : Date
	creadoEn : Date
	creadoPor : number
	modificadoEn : Date
	modificadoPor : number
	activo : boolean
	particular: regBitacoraParticularesModel

	constructor()
	{
		this.id = -1
		this.idVeterano = -1
		this.idBeneficiario = -1
		this.idParticular = -1
		this.idUnidad = -1
		this.idActividad = -1
		this.idCanalAtencion = -1
		this.idEstatus = -1
		this.tipoLlamada = false
		this.fecha = new Date()
		this.descripcion = ''
		this.horaInicio = new Date()
		this.horaFin = new Date()
		this.creadoEn = new Date()
		this.creadoPor = -1
		this.modificadoEn = new Date()
		this.modificadoPor = -1
		this.activo = true
	}
}